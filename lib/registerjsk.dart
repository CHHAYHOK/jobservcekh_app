import 'package:findjob/EditColor.dart';
import 'package:findjob/loginfb.dart';
import 'package:findjob/loginjsk.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart';
import './laoding.dart';
import './verifyCode.dart';
import './detailjob.dart';
import 'package:toast/toast.dart';


class registerjsk extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _RegisterPageState();
}

class _RegisterPageState extends State<registerjsk> {
  TextEditingController username = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController conpassword = new TextEditingController();
  TextEditingController phone = new TextEditingController();

  void insert() {
    var url = "http://192.168.0.103:8080/Test/insert.php";
    http.post(url, body: {
      "username": username.text,
      "email": email.text,
      "password": password.text,
      "conpassword": conpassword.text,
      "phone":phone.text,
    });
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Register Job Steeker'),
        centerTitle: true,
        backgroundColor: EditColor,
      ),
      body: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Card(
          child: ListView(
            children: <Widget>[
             new Container(
            decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                    color: Colors.grey[350],
                    width: 1.0,
                  )
              ),
            ),
            height: 120,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                  child: Center(child: Text("REGISTER",style: TextStyle(fontSize: 25,color: EditColor),)),
                  //child: Image.asset("images/logo.png",)
                  
              ),
          ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: new Form(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: new TextField(
                          autofocus: true,
                          decoration: new InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Username',
                              contentPadding: EdgeInsets.all(8),
                              prefixIcon: Icon(
                                Icons.person,
                              )), //decoration
                          style: TextStyle(fontSize:17.0, color: Colors.black),
                          controller: username,
                        ),
                      ), //TextFormField

                      Padding(
                        padding: const EdgeInsets.only(top: 12.0),
                        child: new TextField(
                          decoration: new InputDecoration(
                             contentPadding: EdgeInsets.all(8),
                              border: OutlineInputBorder(),
                              labelText: 'Email',
                              prefixIcon: Icon(Icons.email,)),
                          style: TextStyle(fontSize: 17.0, color: Colors.black),
                          controller: email,
                        ),
                      ), //TextFormField

                      Padding(
                        padding: const EdgeInsets.only(top: 12.0),
                        child: new TextField(
                          decoration: new InputDecoration(
                             contentPadding: EdgeInsets.all(8),
                              border: OutlineInputBorder(),
                              labelText: 'Mobile Phone',
                              prefixIcon: Icon(Icons.phone_iphone,)),
                          style: TextStyle(fontSize: 17.0, color: Colors.black),
                          controller: phone,
                          keyboardType: TextInputType.phone,
                        ),
                      ), //TextFormField

                      Padding(
                        padding: const EdgeInsets.only(top: 12.0),
                        child: new TextField(
                          decoration: new InputDecoration(
                             contentPadding: EdgeInsets.all(8),
                              border: OutlineInputBorder(),
                            prefixIcon: Icon(Icons.lock,),
                            labelText: 'Password',
                          ),
                          controller: password,
                          obscureText: true,
                          style: TextStyle(fontSize: 17.0, color: Colors.black),
                        ),
                      ), //TextFormField

                      Padding(
                        padding: const EdgeInsets.only(top: 12.0),
                        child: new TextField(
                          decoration: new InputDecoration(
                             contentPadding: EdgeInsets.all(8),
                              border: OutlineInputBorder(),
                            prefixIcon: Icon(Icons.lock_open,),
                            labelText: 'Confirm Password',
                          ),
                          controller: conpassword,
                          obscureText: true,
                          style: TextStyle(fontSize: 17.0, color: Colors.black),
                        ),
                      ), //TextFormField


                        Padding(
                          padding: const EdgeInsets.only(top: 23.0),
                          child: new RaisedButton(
                            color: Colors.amber,
                            padding: EdgeInsets.all(13.0),
                            child: new Text(
                              'CREATE NOW',
                              style: new TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.white,),
                            ),
                            onPressed: () {
                              //insert();
                              // conpassword.clear();
                              // phone.clear();
                              // email.clear();
                              // username.clear();    
                              // password.clear();
                               sendPhoneCode();
                              // Navigator.pop(context);
                            },
                          ),
                        ), //Padding

                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: Text(
                            "Register by using your Facebook account",
                            style: TextStyle(fontSize: 15.0),
                          ),
                        ),
                      ),

                      
                        InkWell(
                            onTap: (){
                             Navigator.pushNamed(context, "/loginfb");
                            },
                        child:Center(
                        child: Container(
                          width: 200,
                          color: Colors.blue[900],
                          margin: EdgeInsets.only(top: 20.0),
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: new Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 20.0),
                                  child: new Image.asset(
                                    "images/face.png",
                                    width: 30.0,
                                    color: Colors.white,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 15.0),
                                  child: Text("Facebook",
                                      style: TextStyle(
                                          fontSize: 18.0, color: Colors.white)),
                                ),
                              ],
                            ),
                          ),
                        ), //Container
                         ),
                      ), //Center
                    ], //Widget
                  ), //Column
                ), //Form
              ),
            ],
          ),
        ),
      ),
    ); //Scaffold
  }
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String verificationId;
  sendPhoneCode(){
    loading(context);
    final PhoneVerificationCompleted verificationCompleted = (AuthCredential credentail) {
      Navigator.pop(context);
      Navigator.of(context).push(
          MaterialPageRoute(
              builder: (BuildContext ctx)=>loginjsk()
          )
      );
      print('Inside _sendCodeToPhoneNumber: signInWithPhoneNumber auto succeeded: $credentail');
    };

    final PhoneVerificationFailed verificationFailed = (AuthException authException) {
      Navigator.pop(context);
      Toast.show("The number format you enter is not correct.", context,duration: 3);
    };

    final PhoneCodeSent codeSent = (String verificationId, [int forceResendingToken]) async {
      this.verificationId = verificationId;
      Navigator.pop(context);
      Navigator.of(context).push(
          MaterialPageRoute(
              builder: (BuildContext ctx)=> VerifyCord(verificationId: this.verificationId,phone: this.phone.text,)
          )
      );
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout = (String verificationId) {
      this.verificationId = verificationId;
      print("time out");
    };
    _auth.verifyPhoneNumber(
        codeSent: codeSent,
        phoneNumber: this.phone.text,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        timeout: Duration(seconds: 60)
    );

  }
}
