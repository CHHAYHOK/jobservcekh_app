import 'package:findjob/API/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'dart:async';
import 'dart:convert';
import './detailjob.dart';
import 'package:http/http.dart' as http;
import './EditColor.dart';
import './appbar.dart';

class listjob extends StatefulWidget {
  listjobState createState() => new listjobState();

  
}


class listjobState extends State<listjob> with SingleTickerProviderStateMixin {
  MyAPI _myAPI = new MyAPI();
  bool isListClicked = false;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(
        title: Center(child: Text("JOB LIST")),
        backgroundColor:EditColor,
  
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: new IconButton(
                icon: Icon(Icons.search),
                iconSize: 30.0,
                onPressed: () {
                  Navigator.of(context).pushNamed("/searchjob");
                }),
          ),
        ],
      ),
      backgroundColor: Colors.grey[300],
      body: SingleChildScrollView(
        child: Column(
           children: <Widget>[
             Container(
      height: 150,
      margin: EdgeInsets.symmetric(vertical:10),
      child: new Swiper(
        autoplay: true,
      itemBuilder: (BuildContext context, int index) {

        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            image: DecorationImage(image:AssetImage("images/job.jpeg"),fit: BoxFit.cover ),
            // image: DecorationImage(image: NetworkImage (MyAPI().imgurl+data.companyLogo))
          ),
          );
          },
      itemCount: 10,
      viewportFraction: 0.8,
      scale: 0.9,
),
    ),

            new Container(
              
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: Colors.grey[300]
                ),
                 color: Colors.white,
              ),
             
              child: Padding(
              padding: const EdgeInsets.only(left: 15.0),
              child: Row(
                children: <Widget>[
                  new Text(
                    "Latest Jobs",
                    style: TextStyle(fontSize: 20),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 180.0,
                    ),
                    child: isListClicked
                        ? IconButton(
                            icon: Icon(
                              Icons.view_module,
                              size: 30,
                            ),
                            onPressed: () {
                              setState(() {
                                isListClicked = false;
                              });
                              // Navigator.of(context).pushNamed("/dasicon");
                            })///IconButton
                        : IconButton(
                            icon: Icon(
                              Icons.view_list,
                              size: 30,
                            ),
                            onPressed: () {
                              setState(() {
                                isListClicked = true; 
                              });
                              // Navigator.of(context).pushNamed("/dasicon");
                             
                            }),//IconButton
                  ),
                ],
              ),
            )
            ), //Card

            new FutureBuilder(
              future: _myAPI.getJobList(),
              builder: (context, snapshot) {
                if (snapshot.hasError) print(snapshot.error);
                else if(snapshot.hasData){
                  Joblist list = snapshot.data;
                  return isListClicked? ItemGrid(list: list.data.data,) 
                  : ItemList(list: list.data.data,);
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ), //container
          ],
        ),
      ),
    );
  }
}



class ItemList extends StatelessWidget {
  final List<JobData> list;

  static bool like;

  ItemList({this.list});
  
bool expandFlag = false;
  Widget build(BuildContext context) {
    return new Column(
      children: list.map((data){
        return GestureDetector(
          onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new Detail(
                        jobid: data.id.toString(),
                      )) //MaterialPageRoute
              ),
             child: Container(
               color: Colors.white,
               height: 130,
               margin: EdgeInsets.symmetric(vertical: 4.0),
               child: Row(
                 children: <Widget>[
                   SizedBox(width: 10,),
                   new Container(
                     height: 100,
                     width: 100,
                     decoration: BoxDecoration(
                       image: DecorationImage(image: NetworkImage (MyAPI().imgurl+data.companyLogo))
                     ),

                   ),
                   SizedBox(width: 15,),

                   Expanded(
                     child: new Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                         new Text(
                        "${data.jobTitle}",
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold,color: EditColor),
                      ),
                      SizedBox(height:5.0,),
                     new Text(
                         " ${data.locationName}",style: TextStyle(fontSize: 14,color: Colors.black45),
                               
                       ),
                       SizedBox(height:5.0,),
                       new Text(
                         " ${data.createDate}",style: TextStyle(fontSize: 12,color: Colors.black45),
                               
                       ),
                       ],
                     ),
                   )
                 ],
               ),
             ),
        );
      }).toList(),
    );
  }
}

class ItemGrid extends StatelessWidget {
  final List<JobData> list;

  static bool like;

  ItemGrid({this.list});


  Widget build(BuildContext context) {
    
    return  new GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,crossAxisSpacing: 10),
      controller: ScrollController(keepScrollOffset: false),
      shrinkWrap: true,
      itemCount: list == null ? 0 : list.length,
      itemBuilder: (context, i) {
        return GestureDetector(
          onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new Detail(
                       jobid: list[i].id.toString(),
                      )) //MaterialPageRoute
              ),
             child: Container(
               color: Colors.white,
               margin: EdgeInsets.symmetric(vertical: 4.0),

               child: Column(
                 children: <Widget>[
                   new Container(
                     height: 100,
                     width: 100,
                     decoration: BoxDecoration(
                       image: DecorationImage(image: NetworkImage (MyAPI().imgurl+list[i].companyLogo))
                     ),

                   ),
                   SizedBox(width: 15,),
                   Expanded(
                     child: new Column(
                       crossAxisAlignment: CrossAxisAlignment.center,
                       children: <Widget>[
                         new Text(
                        "${list[i].jobTitle}",
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold,color: EditColor),
                      ),
                      SizedBox(height:5.0,),
                     new Text(
                         " ${list[i].locationName}",style: TextStyle(fontSize: 14,color: Colors.black45),
                               
                       ),
                       SizedBox(height:5.0,),
                       new Text(
                         " ${list[i].createDate}",style: TextStyle(fontSize: 12,color: Colors.black45),
                               
                       ),
                       ],
                     ),
                   )
                 ],
               ),
             ),
        );
      },
    ); //GridView
  }
}

class DataSearch extends SearchDelegate<String> {
  final cities = [
    
  ];
  final recentCities = [
     
  ];
  @override
  List<Widget> buildActions(BuildContext context) {
    // TODO: implement buildActions
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = "";
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    return null;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty ? recentCities : cities;

    return ListView.builder(
      itemBuilder: (context, index) => ListTile(
            leading: Icon(Icons.home),
            title: Text(suggestionList[index]),
          ),
      itemCount: suggestionList.length,
    );
  }
}
