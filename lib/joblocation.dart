import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import './detailjob.dart';
import 'package:http/http.dart' as http;
import './EditColor.dart';

class Joblocation extends StatefulWidget {
  JoblocationState createState() => new JoblocationState();

  
}


class JoblocationState extends State<Joblocation> with SingleTickerProviderStateMixin {
  Future<List> getData() async {
    final response =
        await http.get("http://192.168.0.107:8080/job_api/job_list.php");
    return json.decode(response.body);
  }
  bool isListClicked = false;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(
        title: Center(child: Text("Job Location")),backgroundColor: EditColor,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: new IconButton(
                icon: Icon(Icons.search),
                iconSize: 30.0,
                onPressed: () {
                  showSearch(context: context, delegate: DataSearch());
                }),
          ),
        ],
      ),
    
      body: ListView(
        controller: ScrollController(keepScrollOffset: false),
        shrinkWrap: true,
        children: <Widget>[
        new FutureBuilder<List>(
              future: getData(),
              builder: (context, snapshot) {
                if (snapshot.hasError) print(snapshot.error);
                return snapshot.hasData
                    ? new ItemList(list: snapshot.data)
                    : new Center(
                        child: new CircularProgressIndicator(),
                      );
              },
            ),
           //container
        ],
      ),
    );
  }
}



class ItemList extends StatelessWidget {
  final List list;

  static bool like;

  ItemList({this.list});
  
bool expandFlag = false;

  Widget build(BuildContext context) {
  
    return new ListView.builder(
      controller: ScrollController(keepScrollOffset: false),
      shrinkWrap: true,
      itemCount: list == null ? 0 : list.length,
      itemBuilder: (context, i) {
        return GestureDetector(
          onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new Detail(
                        jobid:list[i],
                      )) //MaterialPageRoute
              ),
          child: new Card(
                child: new ListTile(
                  title: Padding(
                    padding: const EdgeInsets.all(10),
                    child: new Text(
                      list[i]['company_address'],
                      style: TextStyle(
                          fontSize: 18.0, color: EditColor),
                    ),
                  ), //title
                 trailing: Container(
                  height: 40.0,
                  width: 40.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.blue,
                  ),
                  child: Center(
                      child: new Text(
                    "10",
                    style: TextStyle(fontSize: 18.0, color: Colors.white),
                  )),
                ),
                ), //ListTile
             
          ), //Card
        );
      },
    );
  }
}

class DataSearch extends SearchDelegate<String> {
  final cities = [
    "Home",
    "Hello",
    "Welcome",
    "Hi",
    "Free",
    "Monday",
    "Tuesday",
    "wedneday",
    "Thursday",
    "Friday",
    "Satuday",
    "Sunday"
  ];
  final recentCities = [
    "Monday",
    "Tuesday",
    "wedneday",
    "Thursday",
    "Friday",
    "Satuday",
    "Sunday"
  ];
  @override
  List<Widget> buildActions(BuildContext context) {
    // TODO: implement buildActions
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = "";
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    return null;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty ? recentCities : cities;

    return ListView.builder(
      itemBuilder: (context, index) => ListTile(
            leading: Icon(Icons.home),
            title: Text(suggestionList[index]),
          ),
      itemCount: suggestionList.length,
    );
  }
}
