import 'package:findjob/jobsteeker.dart';
import 'package:findjob/laoding.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import './EditColor.dart';

class VerifyCord extends StatefulWidget {
  final String verificationId;
  final String phone;
  VerifyCord({
    this.verificationId,this.phone,
});
  @override
  State<StatefulWidget> createState() => new _VerifyCordPageState();

}

class _VerifyCordPageState extends State<VerifyCord>{

  FirebaseAuth _auth;
  final _scaffoldKey= new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Center(child: Text("Verification"),), 
      backgroundColor: EditColor,),
      key: _scaffoldKey,
      body: ListView(
        children: <Widget>[
          new Container(
            margin: EdgeInsets.only(top: 50),
             height: 100,
            child: Image.asset("images/verify1.png",color: Colors.redAccent,),
            
          ),
          new Container(
                  child: Column(
                    children: <Widget>[
                      new Text("Verification Code",style:TextStyle(fontSize: 18,color: Colors.black),)
                       
                    ],
                  )
              
          ),
          new Container(
            margin: EdgeInsets.only(top: 20),
                  child: Row(
                    children: <Widget>[
                      Padding(
                           padding: const EdgeInsets.all(10),
                          child: new Text("Enter Verification Codes are 6 digits long",
                           style:TextStyle(fontSize: 15,color: Colors.black),)
                         )
                      
                    ],
                  ),
                  
          ),
          new Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(15.0),
      
            height: 100,
            child: TextField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                 border: OutlineInputBorder(),
                hintText: "Enter code ****",hintStyle: TextStyle(fontSize: 18),
                
              ),
              style: TextStyle(fontSize: 18),
              onChanged: (code){
                if(code.length==6){
                  verifyCode(code: code);
                }
              },
            ),
          )
        ],
      ),
    );

  }
  verifyCode({String code}){
    loading(context);
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: widget.verificationId,smsCode: code??"0"
    );

    _auth.signInWithCredential(credential).then((onValue){
      Navigator.pop(context);
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext ctx)=>jobsteeker(),
        )
      );
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("Successfully"),
        )
      );
    }).catchError((onError){
      Navigator.pop(context);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("Incorrect code"),
        )
      );
    });
  }
}