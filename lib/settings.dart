import 'package:findjob/EditColor.dart';
import 'package:flutter/material.dart';
import './EditColor.dart';

class Settings extends StatefulWidget{
  State<StatefulWidget> createState()=> _SettingsPageState();
}
class _SettingsPageState extends State<Settings>{
  bool value1 = true;
  bool value2 = false;

  void onChangedValue(bool value){
    setState(() {
      value1 = value; 
    });
  }
@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:Center(child: Text("Settings")),backgroundColor:EditColor,),   
        body: ListView(
          children: <Widget>[
            new Container(
              
               child: Column(
                 children: <Widget>[
                   Card(
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: new ListTile(
              leading: Container(
                  height: 50.0,
                  width: 60.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: Colors.grey[800],
                      ),
                      color: Colors.white),
                  child: Icon(
                    Icons.notifications,
                    size: 35.0,
                    color: Colors.grey[800],
                  )), //Container

              title: Text(
                "Notification",
                style: TextStyle(fontSize: 20.0, color: Colors.black87,),
              ),
            trailing: Switch(value:value1 , onChanged: onChangedValue,),
            ),
          ),
        ), //Card
           InkWell(
             onTap: (){
              Navigator.of(context).pushNamed("/changepassword");             },
            child: new Card(
          child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: new ListTile(
                leading: Container(
                    height: 50.0,
                    width: 60.0,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                          color: Colors.grey[800],
                        ),
                        color: Colors.white),
                    child: Icon(
                      Icons.lock_outline,
                      size: 35.0,
                      color: Colors.grey[800],
                    )), //Container

                title: Text(
                  "Changs Password",
                  style: TextStyle(fontSize: 20.0, color: Colors.black87,),
                ),
              ),
          ),
        ),
           ), //Card
         new Card(
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: new ListTile(
              leading: Container(
                  height: 50.0,
                  width: 60.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: Colors.grey[800],
                      ),
                      color: Colors.white),
                  child: Icon(
                    Icons.exit_to_app,
                    size: 35.0,
                    color: Colors.grey[800],
                  )), //Container

              title: Text(
                "Logout",
                style: TextStyle(fontSize: 20.0, color: Colors.black87,),
              ),
             
            ),
          ),
        ), //Card
                 ],
               ),
            )
            
          ],
        ),
    ) ;
  }
}