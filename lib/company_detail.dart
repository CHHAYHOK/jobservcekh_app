import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';
import './EditColor.dart';
import 'dart:async';
import 'package:flutter_html/flutter_html.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import 'API/api.dart';

class Company_detail extends StatefulWidget {
  List list;
  int index;
  Company_detail({this.index, this.list});
  
  _Company_detailState createState() => new _Company_detailState();


}


class _Company_detailState extends State<Company_detail>with SingleTickerProviderStateMixin  {
   MyAPI _myAPI = new MyAPI();
  TabController controller;
  @override
  void initState() {
    super.initState();
    controller = new TabController(vsync: this, length: 5);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
     
      appBar: AppBar(
        title: Center(child: Text("Company_detail Job")),
        backgroundColor: EditColor,

        leading: InkWell(
          onTap: ()=>Navigator.pop(context),
          child: Row(
            children: <Widget>[
              Icon(Icons.arrow_back_ios),
              Text("Back")
            ],
          ),
        ),

       actions: <Widget>[
        IconButton(
          icon: Icon(Icons.turned_in),
          onPressed: (){

          },
        ),
         IconButton(
          icon: Icon(Icons.share),
          onPressed: (){
           Share.share('http://172.20.10.2:8080/easy_job/public/job_detail/');
          },
        ),

       ],

      ),
 
      body: new ListView(

        children: <Widget>[
           Container(
               color: Colors.white,
               height: 130,
               margin: EdgeInsets.symmetric(vertical: 4.0),
               child: Row(
                 children: <Widget>[
                   SizedBox(width: 10,),
                   new Container(
                     height: 100,
                     width: 100,
                     decoration: BoxDecoration(
                       image: DecorationImage(image: AssetImage ("images/amret.gif"))
                     ),

                   ),
                   SizedBox(width: 15,),

                   Expanded(
                     child: new Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                         new Text( 
                        " ${widget.list[widget.index]['companyName']}",
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold,color: EditColor),
                      ),
                     
                  
                       ],
                     ),
                   )
                 ],
               ),
             ),
          
        ],

      ), //ListView
  
    );
     //Scaffold
  }
}

