import 'package:findjob/API/api.dart';
import 'package:findjob/EditColor.dart';
import 'package:findjob/detailjob.dart';
import 'package:flutter/material.dart';
import './EditColor.dart';

class Searchjob extends StatefulWidget{

  @override
  State<StatefulWidget>createState()=>new _SearchjobPageState();
}
class _SearchjobPageState extends State<Searchjob>{

String data;
MyAPI myAPI = new MyAPI();
TextEditingController search = new TextEditingController();
@override
 Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar( 
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: ()=>Navigator.pop(context),
          icon: Icon(Icons.arrow_back),
          color: Colors.black,
        ),
        title: new TextField(  
          autofocus: true,  
          controller: search,                    
          style: TextStyle(fontSize: 20),
            decoration: new InputDecoration( 
                hintText:("All Search"),
                border: InputBorder.none
                
            ),
            
            onChanged: (text){
              setState(() {
               data= text; 
              });
            },
          ),
          actions: <Widget>[
           data==null? Container(): IconButton(
              color: Colors.red,
              icon: Icon(Icons.close),
              onPressed: (){
                setState(() {
                 data=null; 
                 search.clear();
                });
              },
            )
          ],
        ),
        backgroundColor: Color(0xFFeaeaea),
        body: data ==null ? 
        Center(
          child: Text("Enter some text to search"),
        )
        : FutureBuilder(
          future: myAPI.search(
            data: data
          ),
          builder: (_ , snap){
            if(snap.hasData){
              JobDetail result = snap.data;
              List<JobDeaildata> results = result.jobdata;
              return results.length >0 ? ListView.builder(
                itemCount: results.length,
                itemBuilder: (context, i){
                  return GestureDetector(
                    onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context) => new Detail(
                                  jobid: results[i].id.toString(),
                                )) //MaterialPageRoute
                        ),
                      child: Container(
                        color: Colors.white,
                        height: 130,
                        margin: EdgeInsets.symmetric(vertical: 4.0),
                        child: Row(
                          children: <Widget>[
                            SizedBox(width: 10,),
                            new Container(
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                image: DecorationImage(image: NetworkImage (MyAPI().imgurl+results[i].companyLogo))
                              ),

                            ),
                            SizedBox(width: 15,),

                            Expanded(
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Text(
                                  "${results[i].jobTitle}",
                                  style: TextStyle(
                                      fontSize: 16.0, fontWeight: FontWeight.bold,color: EditColor),
                                ),
                                SizedBox(height:5.0,),
                              new Text(
                                  " ${results[i].locationName}",style: TextStyle(fontSize: 14,color: Colors.black45),
                                        
                                ),
                                SizedBox(height:5.0,),
                                new Text(
                                  " ${results[i].createDate}",style: TextStyle(fontSize: 12,color: Colors.black45),
                                        
                                ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                  );
                },
              ) : Center(
                child: Text("No result Fount",style: TextStyle(fontSize: 18,color: Colors.red),),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
    );
 }

}