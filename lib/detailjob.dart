import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher.dart';
import './EditColor.dart';
import 'dart:async';
import 'package:flutter_html/flutter_html.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import 'API/api.dart';

class Detail extends StatefulWidget {
  String jobid;
  Detail({
    this.jobid,
  });

  _DetailState createState() => new _DetailState();
}

class _DetailState extends State<Detail> with SingleTickerProviderStateMixin {
  MyAPI _myAPI = new MyAPI();
  String phonenumber;
  TabController controller;
  @override
  void initState() {
    super.initState();
    controller = new TabController(vsync: this, length: 5);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(
        title: Center(child: Text("Detail Job")),
        backgroundColor: EditColor,
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          child: Row(
            children: <Widget>[Icon(Icons.arrow_back_ios), Text("Back")],
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.turned_in),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () {
              Share.share(
                  'http://192.168.1.4:8080/easy_job/public/job-details/${widget.jobid}');
            },
          ),
        ],
      ),
      body: new FutureBuilder(
        future: _myAPI.getJobDetail(widget.jobid),
        builder: (context, data) {
          if (data.hasData) {
            JobDetail detail = data.data;
            JobDeaildata list = detail.jobdata[0];
            phonenumber=list.companyPhoneNumber;
            print("List data $list");
            return ListView(
              children: <Widget>[
                Center(
                  child: new Container(
                    height: 200.0,

                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.grey[500],
                          width: 1.0,
                        ),
                      ),
                      color: Colors.white,
                    ),

                    child: Center(
                        child: Image.network(
                       "${MyAPI().imgurl}${list.companyLogo}",
                      width: 150,
                    )),
//                  child: new Image.network(
//                  "http://192.168.43.58:8080/jobsevice/uploads/company-logo/${widget.list[widget.index]['company_logo']}")
                  ),
                ),

                // new Icon(Icons.turned_in,size: 60,color: Colors.grey,),
                new Card(
                  child: Column(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: Colors.grey[300],
                            ),
                            borderRadius: BorderRadius.circular(5)),
                        child: new ListTile(
                          title: new Text(
                            " ${list.jobTitle}",
                            style:
                                new TextStyle(fontSize: 20.0, color: EditColor),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: new Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: new Icon(Icons.event_busy),
                            ),
                            new Text(" Closing Date:  ${list.closeDate}"),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                new Card(
                  child: Column(
                    children: <Widget>[
                      new Divider(height:5.1,),
                       Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          children: <Widget>[
                            new Icon(Icons.account_balance,size: 17,),
                            new Text(
                              " ${ list.companyName}",
                              style: new TextStyle(fontSize: 15.0),
                            ),
                          ],
                        ),
                      ),
                      new Divider(height:5.1,),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          children: <Widget>[
                            new Icon(Icons.location_on,size: 17,),
                            new Text(
                              " ${list.locationName}",
                              style: new TextStyle(fontSize: 15.0),
                            ),
                          ],
                        ),
                      ),
                     new Divider(height:5.1,),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          children: <Widget>[
                            new Icon(Icons.phone,size: 17),
                            new Text(
                              " ${list.companyPhoneNumber}",
                              style: TextStyle(fontSize: 15),
                            ),
                          ],
                        ),
                      ),
                      new Divider(height:5.1,),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          children: <Widget>[
                            new Icon(Icons.email,size: 17),
                            new Text(
                              " ${list.companyEmail}",
                              style: TextStyle(fontSize: 15),
                            ),
                          ],
                        ),
                      ),
                      new Divider(height:5.1,),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          children: <Widget>[
                            new Icon(Icons.access_time,size: 17),
                            new Text(
                              " Schedule :  ${list.scheduleName}",
                              style: new TextStyle(fontSize: 15.0),
                            ),
                          ],
                        ),
                      ),
                      new Divider(height:5.1,),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          children: <Widget>[
                            new Icon(Icons.attach_money,size: 17),
                            new Text(
                              " ${list.salary}",
                              style: new TextStyle(fontSize: 15.0),
                            ),
                          ],
                        ),
                      ),
                     new Divider(height:0.1,),
                    ], //Widget
                  ), //Column
                ), //Card
                new Card(
                  child: Column(
                    children: <Widget>[
                      new ListTile(
                        title: new Text(
                          " Description ",
                          style: TextStyle(
                              fontSize: 17.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                      new ListTile(
                        //leading: new Icon(Icons.done),
                        title: new Html(
                          data: " ${list.requirement}",
                        ),
                      )
                    ],
                  ),
                ),
                
              ],
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              new MyButton(
                title: "Call",
                colors: Colors.blue,
                iconData: Icons.call,
                ontap: () {
                  launch("tel://$phonenumber");
                },
              ),
              new MyButton(
                title: "Apply Now",
                colors: Colors.amber,
                iconData: Icons.launch,
                ontap: () {
                  Navigator.of(context).pushNamed("/loginjsk");
                },
              ),
            ],
          ),
        ),
      ),
    );
    //Scaffold
  }
}

class MyButton extends StatelessWidget {
  final String title;
  final Color colors;
  final IconData iconData;
  final GestureTapCallback ontap;
  MyButton(
      {@required this.title,
      this.colors = Colors.teal,
      this.iconData,
      @required this.ontap});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        ontap();
      },
      child: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width / 2 - 30,
        margin: EdgeInsets.symmetric(horizontal: 5.0),
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              iconData,
              size: 20,
              color: Colors.white,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              "$title",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
        decoration: BoxDecoration(
            color: colors, borderRadius: BorderRadius.circular(50)),
      ),
    );
  }
}
