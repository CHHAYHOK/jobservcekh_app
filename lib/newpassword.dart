import 'package:findjob/EditColor.dart';
import 'package:flutter/material.dart';

class Newpass extends StatefulWidget{

  State<StatefulWidget>createState()=> new _NewpassState();

}
class _NewpassState extends State<Newpass>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(

      appBar: AppBar(title: Text("New Password"),backgroundColor: EditColor,),

      body: ListView(
        children: <Widget>[
          new Container(
            margin: EdgeInsets.only(top: 50),
            height: 200,
            child: Form(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: new TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15),
                        border: OutlineInputBorder(),
                       hintText: "Enter New Password",
                      ),
                      style: TextStyle(fontSize: 18),
                     
                    ),
                  ),
                   Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: new TextField(
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15),
                        border: OutlineInputBorder(),
                       hintText: "Retype New"
                       
                      ),
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Padding(
                        padding: const EdgeInsets.only(top: 23.0),
                        child: Container(
                          width: 390,
                          child: new RaisedButton(
                            color: Colors.amber,
                            padding: EdgeInsets.all(13.0),
                            child: new Text(
                              'Continues',
                              style: new TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.white,),
                            ),
                            onPressed: () {
                            
                              // Navigator.pop(context);
                            },
                          ),
                        ),
                      ), 
                 
                ],
              ),
            ),
            
          )
        ],
      ),
    );
  }
}