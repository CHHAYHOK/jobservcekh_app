import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './EditColor.dart';

class loginjsk extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginjskPageState();
}

class _LoginjskPageState extends State<loginjsk> {
  //   dialog = new AlertDialog(
  //   content: new Text("Hello"),
  //   title: new Text("REGISTER"),
 
  // );
  TextEditingController email = new TextEditingController();
  TextEditingController password = new TextEditingController();
  String msg='';

  Future <List>login() async {
    final response =
        await http.post("http://192.168.1.4:8080/findjob/loginjsk.php", body: {
      "email": email.text,
      "password": password.text,
    });
    var datauser = json.decode(response.body);

    if(datauser.length==0){
      setState(() {
             msg="Login Fail";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Login Job Seeker')),
        backgroundColor: EditColor,
      ),
      body: ListView(
        controller: ScrollController(keepScrollOffset: false),
        shrinkWrap: true,
        children: <Widget>[
          new Container(
            decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                    color: Colors.grey[350],
                    width: 1.0,
                  )
              ),
            ),
            height: 120,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                  child: Center(child: Text("SIGN IN",style: TextStyle(fontSize: 25,color: EditColor),)),
                  //child: Image.asset("images/logo.png",)
                  
              ),
          ),
          
             
               Padding(
                 padding: const EdgeInsets.all(20.0),
                 child: new Container(
                   padding: EdgeInsets.all(10.0),
                  child: new Form(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: new TextField(
                            autofocus: true,
                            decoration: new InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Phone Number Or Email',labelStyle: TextStyle(fontSize: 15),
                                contentPadding: EdgeInsets.all(8),
                                prefixIcon: Icon(
                                  Icons.person,
                                )), //decoration
                            style: TextStyle(fontSize: 18.0, color: Colors.black),
                            controller: email,
                          ),
                        ), //TextFormField

                       
                        Padding(
                          padding: const EdgeInsets.only(top: 12.0),
                          child: new TextField(
                            decoration: new InputDecoration(
                               contentPadding: EdgeInsets.all(8),
                                border: OutlineInputBorder(),
                                labelText: 'Password',labelStyle:TextStyle(fontSize: 15),
                                prefixIcon: Icon(Icons.lock_outline,)),
                            style: TextStyle(fontSize: 18.0, color: Colors.black),
                             obscureText: true,
                            controller: password,
                          ),
                        ), //
                        Padding(
                          padding: const EdgeInsets.only(top: 25.0),
                          child: new RaisedButton(
                              color: Colors.amber,
                              padding: EdgeInsets.all(10.0),
                              child: new Text(
                                'LOG IN',
                                style: new TextStyle(
                                    fontSize: 15.0, color: Colors.white),
                              ),
                              onPressed: () {
                                if(email.text=="abc" && password.text=="123")
                                {
                                  Navigator.pushNamed(context, "/jobsteeker");
                                }
                                else{
                                  print("Incorrect");
                                }
                                password.clear();
                                email.clear();
                              }
                          ), //RainsedButton
                        ),

                        //Text(msg,style: TextStyle(fontSize: 18.0,color: Colors.red),),
                        
                           Padding(
                            padding: const EdgeInsets.only(top:25.0),
                            child: InkWell(
                              onTap: (){
                                Navigator.of(context).pushNamed("/forgetpass");                              },
                              child: Center(
                                child: Container(
                                  width: 200,
                                  
                                  child: new Text(
                                    "Forget Password?",
                                    style: TextStyle(fontSize: 15.0, color: Colors.blue),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        
                        //Text(msg,style: TextStyle(fontSize: 20.0,color: Colors.red),),

                        Padding(
                          padding: const EdgeInsets.only(top:25.0),
                          child: new RaisedButton(
                            color: EditColor,
                            padding: EdgeInsets.all(10.0),
                            child: new Text(
                              'CREATE ACCOUNT',
                              style: new TextStyle(
                                  fontSize: 15.0, color: Colors.white),
                            ),
                            onPressed: (){
                              Navigator.pushNamed(context, "/registerjsk");
                            }
                          ),
                        ),
                      ], //Widget
                    ), //Column
                  ), //Form
              ),
               ),
        ],
      ),
    ); //Scaffold
  } //Widget

}
