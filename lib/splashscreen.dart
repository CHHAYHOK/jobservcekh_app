import 'package:findjob/EditColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'dart:async';
import 'package:flutter/services.dart';


class Splashscreen extends StatefulWidget{

  _SplashscreenState createState()=> new _SplashscreenState();
}
class _SplashscreenState extends State<Splashscreen> {

   void navigationToNextPage() {
    Navigator.pushNamed(context,'/homepage');
  }

  startSplashScreenTimer() async {
    var _duration = new Duration(seconds:7);
    return new Timer(_duration, navigationToNextPage);
  }

  @override
  void initState() {
    super.initState();
    startSplashScreenTimer();
  }
  Color gradientState = Colors.deepOrange[200]; 
  Color gradientEnd = Colors.deepOrange[700];   


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Container(
           
            decoration: BoxDecoration( 
              //color: Colors.white
              gradient:  LinearGradient(
                colors: [
                    Color(0xFF004E73),
                    Color(0xFF7fa6b9),
                ]
              ),
            // gradient: new LinearGradient(colors: [gradientState,gradientEnd],
            // begin: new FractionalOffset(0.0,0.5),
            // end: new FractionalOffset(0.5,0.0),
            // stops: [0.0,1.0],
            // tileMode: TileMode.clamp
            // ),
            ),  
             ),
            Column(
              
               mainAxisAlignment:MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  
                  child: Container(
                    child: Column(
                     mainAxisAlignment:MainAxisAlignment.center,
                      children: <Widget>[                          
                           Image.asset("images/logosp.png",width: 150,),
                           
                          // Padding(
                          //   padding: const EdgeInsets.all(8.0),
                          //     Text("Jobservice",style: 
                          //   TextStyle(fontSize: 25,color: Colors.black,)),
                          // ),
                      ],
                    ),
                  )
                ),
                // Expanded(
                //   flex: 1,
                //   child: Column(
                //     mainAxisAlignment: MainAxisAlignment.center,
                //     children: <Widget>[
                //       CircularProgressIndicator(
                //         valueColor: new AlwaysStoppedAnimation<Color>(Colors.amber),
                //       ),
                //       Padding(
                //         padding: const EdgeInsets.all(15.0),
                //         child: Container(
                        
                //           decoration: BoxDecoration(
                //             borderRadius: BorderRadius.circular(15),
                //             color: Colors.white,
                //           ),
                //           width: 180,
                //           child: Padding(
                //             padding: const EdgeInsets.all(8.0),
                //             child: Center(
                //               child: Text("Loading....",style: TextStyle(color: Colors.black,fontSize:18),
                //               ),
                //             ),
                //           ),
                //         ),
                //       ),
                //       Container(
                //         margin: EdgeInsets.only(top: 50),
                //         child: new Text("Powered by Flutter",
                //         style: TextStyle(fontSize: 15,color: Colors.white,)),
                //       ),
                //     ],
                //   ),
                // ),
            ],)

        ],
      ),
    );
  }
}