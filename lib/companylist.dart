import 'package:findjob/API/api.dart';
import 'package:flutter/material.dart';
import './EditColor.dart';
import 'company_detail.dart';

class Comlist extends StatefulWidget {
  State<StatefulWidget> createState() => new _ComlistPageState();
}

class _ComlistPageState extends State<Comlist> {
  MyAPI _myAPI = new MyAPI();
  @override
  Widget build(BuildContext) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: new AppBar(
        title: Center(child: new Text('Search Company')),
        backgroundColor: EditColor,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right:10.0),
            child: new IconButton(
                icon: Icon(Icons.search),
                iconSize: 30.0,
                onPressed: () {}
            ),
          ),
        ],
      ),
      //backgroundColor: Colors.blueGrey,
      body: ListView(
        controller: ScrollController(keepScrollOffset: false),
        shrinkWrap: true,
        children: <Widget>[
//          Container(
//            color: Colors.deepOrange,
//            height: 200.0,
//          ),
          new Card(
            child: new ListTile(
                title: new Text(
                  "Company List",
                  style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                ),
               ),
          ),
         
            new FutureBuilder(
              future: _myAPI.getJobList(),
              builder: (context, snapshot) {
                if (snapshot.hasError) print(snapshot.error);
                else if(snapshot.hasData){
                  Joblist list = snapshot.data;
                  return Companylist(list: list.data.data,); 
                  
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
          
      
        ],
      ),//body
    ); //Scaffold
  }
}
class Companylist extends StatelessWidget {
  final List<JobData> list;

  static bool like;

  Companylist({this.list});


  Widget build(BuildContext context) {
    
    return  new GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,crossAxisSpacing: 10),
      controller: ScrollController(keepScrollOffset: false),
      shrinkWrap: true,
      itemCount: list == null ? 0 : list.length,
      itemBuilder: (context, i) {
        return GestureDetector(
          onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new Company_detail(
                        // list[i].jobId.toString(),
                      )) //MaterialPageRoute
              ),
             child: Container(
               color: Colors.white,
               margin: EdgeInsets.symmetric(vertical: 4.0),
               child: Column(
                 children: <Widget>[
                   new Container(
                     height: 100,
                     width: 100,
                     decoration: BoxDecoration(
                       image: DecorationImage(image: AssetImage ("images/amret.gif"))
                     ),

                   ),
                   SizedBox(width: 15,),

                   Expanded(
                     child: new Column(
                       crossAxisAlignment: CrossAxisAlignment.center,
                       children: <Widget>[
                         new Text(
                        "${list[i].companyName}",
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold,color: EditColor),
                      ),
                      SizedBox(height: 5,),
                       new Text("15 positions",
                        
                        style: TextStyle(
                            fontSize: 16.0, color:Colors.black54),
                      ),
                    
                       ],
                     ),
                   )
                 ],
               ),
             ),
        );
      },
    ); //GridView
  }
}
