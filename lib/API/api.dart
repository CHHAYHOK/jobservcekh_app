import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
class MyAPI {
  final String mainLink= "http://192.168.1.4:8080/easy_job/public/api/";
  final String imgurl="http://192.168.1.4:8080/easy_job/public";

  Future<Joblist> getJobList() async{
    final response =
        await http.get("$mainLink"+"job_list");
    if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    return Joblist.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load job list');
    }
  }
  Future<JobDetail> getJobDetail(String jobid) async{
    final response =
        await http.get(
          "$mainLink"+"job_details/$jobid",);
    if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    return JobDetail.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load job Detal');
    }
  }

  Future<JobDetail> search({String data}) async{
    final response =
        await http.post(
          "$mainLink"+"search",
          body: {
            "data" : "$data"
          }
        );
    if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    return JobDetail.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load job Detal');
    }
  }

}

class JobDetail{
  bool success;
  List<JobDeaildata> jobdata;
   JobDetail({this.success, this.jobdata,});
   factory JobDetail.fromJson(Map<String,dynamic>json){
       
    List data = json['data'] as List;
    List<JobDeaildata> list = data.map((f)=> JobDeaildata.fromJson(f)).toList();
     return JobDetail(
       success: json['success'],
       jobdata: list
     );

   }
}

class Joblist {
  bool success;
  JobListData data;
  String message;

  Joblist({this.success, this.data, this.message});

  Joblist.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null ? new JobListData.fromJson(json['data']) : null;
    message = json['message'];
  }

}

class JobListData {
  int currentPage;
  List<JobData> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  Null nextPageUrl;
  String path;
  int perPage;
  Null prevPageUrl;
  int to;
  int total;

  JobListData(
      {this.currentPage,
      this.data,
      this.firstPageUrl,
      this.from,
      this.lastPage,
      this.lastPageUrl,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total});

  JobListData.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = new List<JobData>();
      json['data'].forEach((v) {
        data.add(new JobData.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

}

class JobData {

 int id;
  int categoryId;
  int locationId;
  int jobId;
  int companyId;
  int scheduleId;
  Null jobSkillSetId;
  String salary;
  String createDate;
  String duties;
  String requirement;
  String closeDate;
  int postedById;
  String createdAt;
  String updatedAt;
  String jobTitle;
  String companyName;
  String locationName;
  String companyLogo;

  JobData(
      {this.id,
      this.categoryId,
      this.locationId,
      this.jobId,
      this.companyId,
      this.scheduleId,
      this.jobSkillSetId,
      this.salary,
      this.createDate,
      this.duties,
      this.requirement,
      this.closeDate,
      this.postedById,
      this.createdAt,
      this.updatedAt,
      this.jobTitle,
      this.companyName,
      this.locationName,
      this.companyLogo,
      });

  JobData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    categoryId = json['category_id'];
    locationId = json['location_id'];
    jobId = json['job_id'];
    companyId = json['company_id'];
    scheduleId = json['schedule_id'];
    jobSkillSetId = json['job_skill_set_id'];
    salary = json['salary'];
    createDate = json['create_date'];
    duties = json['duties'].toString();
    requirement = json['requirement'];
    closeDate = json['close_date'];
    postedById = json['posted_by_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    jobTitle = json['job_title'];
    companyName = json['company_name'];
    locationName = json['location_name'];
     companyLogo = json['company_logo'];
  }
  
}
class JobDeaildata {
   int id;
  int categoryId;
  int locationId;
  int jobId;
  int companyId;
  int scheduleId;
  Null jobSkillSetId;
  String salary;
  String createDate;
  String duties;
  String requirement;
  String closeDate;
  int postedById;
  String createdAt;
  String updatedAt;
  String companyName;
  String companyEmail;
  String companyPhoneNumber;
  String companyWebsiteUrl;
  String locationName;
  String scheduleName;
  String jobTitle;
  String companyLogo;
  

  JobDeaildata(
      {this.id,
      this.categoryId,
      this.locationId,
      this.jobId,
      this.companyId,
      this.scheduleId,
      this.jobSkillSetId,
      this.salary,
      this.createDate,
      this.duties,
      this.requirement,
      this.closeDate,
      this.postedById,
      this.createdAt,
      this.updatedAt,
      this.companyName,
      this.companyEmail,
      this.companyPhoneNumber,
      this.companyWebsiteUrl,
      this.locationName,
      this.scheduleName,
      this.jobTitle,
      this.companyLogo,
      });

  JobDeaildata.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    categoryId = json['category_id'];
    locationId = json['location_id'];
    jobId = json['job_id'];
    companyId = json['company_id'];
    scheduleId = json['schedule_id'];
    jobSkillSetId = json['job_skill_set_id'];
    salary = json['salary'];
    createDate = json['create_date'];
    duties = json['duties'];
    requirement = json['requirement'];
    closeDate = json['close_date'];
    postedById = json['posted_by_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    companyName = json['company_name'];
    companyEmail = json['company_email'];
    companyPhoneNumber = json['company_phone_number'];
    companyWebsiteUrl = json['company_website_url'];
    locationName = json['location_name'];
    scheduleName = json['schedule_name'];
    jobTitle = json['job_title'];
     companyLogo = json['company_logo'];
    
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['category_id'] = this.categoryId;
    data['location_id'] = this.locationId;
    data['job_id'] = this.jobId;
    data['company_id'] = this.companyId;
    data['schedule_id'] = this.scheduleId;
    data['job_skill_set_id'] = this.jobSkillSetId;
    data['salary'] = this.salary;
    data['create_date'] = this.createDate;
    data['duties'] = this.duties;
    data['requirement'] = this.requirement;
    data['close_date'] = this.closeDate;
    data['posted_by_id'] = this.postedById;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['company_name'] = this.companyName;
    data['company_email'] = this.companyEmail;
    data['company_phone_number'] = this.companyPhoneNumber;
    data['company_website_url'] = this.companyWebsiteUrl;
    return data;
  }
}
