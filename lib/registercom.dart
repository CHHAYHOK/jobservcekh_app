// import 'package:flutter/material.dart';

// class register extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => new _RegisterPageState();
// }

// class _RegisterPageState extends State<register> {
//   final formKey = new GlobalKey<FormState>();
//   String _Username;
//   String _Email;
//   String _Password;
//   String _ConPassword;

//   void validateAndSave() {
//     final form = formKey.currentState;
//     if (form.validate()) {
//       form.save();
//       print('Form is valid.Username:$_Username,Password:$_Password');
//       print('Form is valid.Email:$_Email,Password:$_Password');
//     } else {
//       print('Form is Invalid.Email:$_Email,Password:$_Password');
//       print('Form is Invalid.ConPassword:$_ConPassword,Password:$_Password');
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       appBar: new AppBar(
//         title: new Text('Register'),
//         backgroundColor: Colors.lime,
//       ),
//       body: Container(
//         padding: EdgeInsets.all(10.0),
//         child: new Form(
//           key: formKey,
//           child: new Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: <Widget>[
//               Padding(
//                 padding: const EdgeInsets.only(top: 25.0),
//                 child: new TextFormField(
//                   decoration: new InputDecoration(
//                       labelText: 'Username',
//                       border: OutlineInputBorder(
//                         borderRadius: BorderRadius.circular(20.0),
//                       ),
//                       prefixIcon: Icon(Icons.person)),
//                   validator: (value) =>
//                       value.isEmpty ? 'Username can\'t be emty' : null,
//                   onSaved: (value) => _Username = value,
//                   style: TextStyle(fontSize: 20.0, color: Colors.black),
//                 ),
//               ), //TextFormField

//               Padding(
//                 padding: const EdgeInsets.only(top: 25.0),
//                 child: new TextFormField(
//                   decoration: new InputDecoration(
//                       labelText: 'Email',
//                       border: OutlineInputBorder(
//                         borderRadius: BorderRadius.circular(20.0),
//                       ),
//                       prefixIcon: Icon(Icons.email)),
//                   validator: (value) =>
//                       value.isEmpty ? 'Email can\'t be emty' : null,
//                   onSaved: (value) => _Email = value,
//                   style: TextStyle(fontSize: 20.0, color: Colors.black),
//                 ),
//               ), //TextFormField

//               Padding(
//                 padding: const EdgeInsets.only(top: 25.0),
//                 child: new TextFormField(
//                   decoration: new InputDecoration(
//                     border: OutlineInputBorder(
//                       borderRadius: BorderRadius.circular(20.0),
//                     ),
//                     prefixIcon: Icon(Icons.lock),
//                     labelText: 'Password',
//                   ),
//                   obscureText: true,
//                   validator: (value) =>
//                       value.isEmpty ? 'Password can\'t be emty' : null,
//                   onSaved: (value) => _Password = value,
//                   style: TextStyle(fontSize: 20.0, color: Colors.black),
//                 ),
//               ), //TextFormField

//               Padding(
//                 padding: const EdgeInsets.only(top: 25.0),
//                 child: new TextFormField(
//                   decoration: new InputDecoration(
//                     border: OutlineInputBorder(
//                       borderRadius: BorderRadius.circular(20.0),
//                     ),
//                     prefixIcon: Icon(Icons.lock_open),
//                     labelText: 'Confirm Password',
//                   ),
//                   obscureText: true,
//                   validator: (value) =>
//                       value.isEmpty ? 'Confirm Password can\'t be emty' : null,
//                   onSaved: (value) => _ConPassword = value,
//                   style: TextStyle(fontSize: 20.0, color: Colors.black),
//                 ),
//               ), //TextFormField

//               new Row(
//                 children: <Widget>[
//                   Padding(
//                     padding: const EdgeInsets.only(top: 23.0),
//                     child: new RaisedButton(
//                       color: Colors.blueAccent,
//                       padding: EdgeInsets.all(13.0),
//                       child: new Text(
//                         'Register',
//                         style: new TextStyle(
//                             fontSize: 22.0,
//                             color: Colors.white,
//                             fontStyle: FontStyle.italic),
//                       ),
//                       onPressed: () {},
//                     ),
//                   ), //Padding
//                 ],
//               ), //Row

//               new Row(
//                 children: <Widget>[
//                   Padding(
//                     padding: const EdgeInsets.only(top: 23.0),
//                     child: new RaisedButton(
//                       color: Colors.blueAccent,
//                       padding: EdgeInsets.all(13.0),
//                       child: new Text(
//                         'Register',
//                         style: new TextStyle(
//                             fontSize: 22.0,
//                             color: Colors.white,
//                             fontStyle: FontStyle.italic),
//                       ),
//                       onPressed: () {},
//                     ),
//                   ), //Padding
//                 ],
//               ) //Row
//             ], //Widget
//           ), //Column
//         ), //Form
//       ),
//     ); //Scaffold
//   }
// }
