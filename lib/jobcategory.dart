import 'package:flutter/material.dart';
import './EditColor.dart';
class Jobcategory extends StatefulWidget {
  State<StatefulWidget> createState() => new _JobcategoryPageState();
}

class _JobcategoryPageState extends State<Jobcategory> {
  @override
  Widget build(BuildContext) {
    // TODO: implement build
    return Scaffold(
      appBar: new AppBar(
        title: Center(child: new Text('Search Category')),
        backgroundColor: EditColor,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right:10.0),
            child: new IconButton(
                icon: Icon(Icons.search),
                iconSize: 30.0,
                onPressed: () {}
            ),
          ),
        ],
      ),
      //backgroundColor: Colors.blueGrey,
      body: ListView(
        controller: ScrollController(keepScrollOffset: false),
        shrinkWrap: true,
        children: <Widget>[
//          Container(
//            color: Colors.deepOrange,
//            height: 200.0,
//          ),
          new Card(
            child: new ListTile(
                title: new Text(
                  "Job Category",
                  style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold),
                ),
            ),
          ), //Card
          new Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: new ListTile(
                title: new Text(
                  "Accoungting",
                  style: TextStyle(fontSize: 18.0),
                ),
                trailing: Container(
                  height: 40.0,
                  width: 40.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.blue,
                  ),
                  child: Center(
                      child: new Text(
                    "10",
                    style: TextStyle(fontSize: 18.0, color: Colors.white),
                  )),
                ), //Container
              ),
            ),
          ), //Card

          new Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: new ListTile(
                title: new Text(
                  "Assistant",
                  style: TextStyle(fontSize: 18.0),
                ),
                trailing: Container(
                  height: 40.0,
                  width: 40.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.blue,
                  ),
                  child: Center(
                      child: new Text(
                        "18",
                        style: TextStyle(fontSize: 18.0, color: Colors.white),
                      )),
                ), //Container
              ),
            ),
          ), //Card

          new Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: new ListTile(
                title: new Text(
                  "Teacher of Japanese",
                  style: TextStyle(fontSize: 18.0),
                ),
                trailing: Container(
                  height: 40.0,
                  width: 40.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.blue,
                  ),
                  child: Center(
                      child: new Text(
                        "12",
                        style: TextStyle(fontSize: 18.0, color: Colors.white),
                      )),
                ), //Container
              ),
            ),
          ), //Card

          new Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: new ListTile(
                title: new Text(
                  "Sale Agent",
                  style: TextStyle(fontSize: 18.0),
                ),
                trailing: Container(
                  height: 40.0,
                  width: 40.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.blue,
                  ),
                  child: Center(
                      child: new Text(
                        "5",
                        style: TextStyle(fontSize: 18.0, color: Colors.white),
                      )),
                ), //Container
              ),
            ),
          ), //Card

          new Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: new ListTile(
                title: new Text(
                  "Senior Cashier",
                  style: TextStyle(fontSize: 18.0),
                ),
                trailing: Container(
                  height: 40.0,
                  width: 40.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.blue,
                  ),
                  child: Center(
                      child: new Text(
                        "15",
                        style: TextStyle(fontSize: 18.0, color: Colors.white),
                      )),
                ), //Container
              ),
            ),
          ), //Card

        ],
      ), //Body
    ); //Scaffold
  }
}
