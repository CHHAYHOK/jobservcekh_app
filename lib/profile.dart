import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/cupertino.dart';
import './EditColor.dart';

class Profile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _profilePageState();
}

class _profilePageState extends State<Profile> {
  Color containerColor = Colors.white;
  var email = TextEditingController();
  var password = TextEditingController();
  @override
  void dispose() {
    email.dispose();
    password.dispose();
    super.dispose();
  }


  File _image;
  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }
  Future chooseImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
  }
  getphoto(){
    showCupertinoModalPopup(context:
    context,
    builder: (_){
     return CupertinoActionSheet(
        title: Text("Select your Type"),
        actions: <Widget>[
          CupertinoActionSheetAction(
            child: Text("Take Picture"),
            onPressed: (){getImage();}
          ),
          CupertinoActionSheetAction(
            child: Text("Choose Img"),
            onPressed: (){chooseImage();}
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: Text("Cancel",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.red),),
          onPressed: ()=> Navigator.of(context).pop(),
        ),
      );
    }
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(title: Center(child: Text("Manage Your Profile",)),backgroundColor: EditColor,),
      body: ListView(

        children: <Widget>[
          new Container(
          height: 30.0,
      ),
         Container(
           color: Colors.grey[200],
              child: Center(
                child: Column(
                  children: <Widget>[
                    InkWell(
                      onTap: (){
                        getphoto();
                      },

                      child: Stack(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: new Container(
                              height: 150.0,
                              width: 150.0,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: new ExactAssetImage("images/lii.jpg"),
                                  fit: BoxFit.cover,
                                ),
                                shape: BoxShape.circle,
                              ),
                            ), //Container
                          ),//padding
                          Positioned(
                              right: 35.0,
                              bottom: 20.0,
                              width: 20.0,
                              child: Center(
                                child: Container(
                                  child: Icon(
                                    Icons.camera_alt,
                                    size: 35.0,
                                    color: Colors.blueGrey,
                                  ),
                                ),
                              )
                          ),//Positioned

                        ],
                      ),
                    ), //Stack
                    Padding(
                      padding: const EdgeInsets.only(bottom:8.0),
                      child: Text("SET PROFILE PHOTO",style: TextStyle(fontSize: 15.0,color: Colors.blue),),
                    ),
                  ],

                ),

              ), //Column
            ),
          Padding(
            padding: const EdgeInsets.only(top:5),
            child: Card(
              child: new Container(
                height: 350.0,
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 250),
                      child: FlatButton.icon(
                       color: Colors.teal,
                        icon: Icon(Icons.edit,color: Colors.white,), //`Icon` to display
                       label: Text('Edit',style: TextStyle(color: Colors.white),), //`Text` to display
                      onPressed: () {
                        //Code to execute when Floating Action Button is clicked
                        //...
                      },
        ),
                    ),
                     InkWell(
                       onTap: (){
                         setState(() {
                           containerColor == Colors.white?containerColor=Colors.teal[200]
                               : containerColor=Colors.teal[200];
                         });
                       },
                       child: Padding(
                         padding: const EdgeInsets.only(top:5.0),
                         child: Container(
                           height: 70,
                            decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Colors.grey[350],
                        width: 1.0,
                      ),
                    ),
                  ),
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: ListTile(
                                leading: Icon(Icons.person_outline,size: 30.0,),
                                title: Text("Chhayhok",style: TextStyle(fontSize: 15.0),),
                                subtitle: Text("Name",style: TextStyle(fontSize: 13.0),),
                                
                              ),
                            ),

                    ),
                       ),
                     ),//Listtile
            

                     Container(
                       height: 70,
                            decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Colors.grey[350],
                        width: 1.0,
                      ),
                    ),
                  ),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: ListTile(
                            leading: Icon(Icons.phone,size: 30.0,),
                            title: Text("+855 964775603",style: TextStyle(fontSize: 15.0),),
                            subtitle: Text("Phone number",style: TextStyle(fontSize: 13.0),),
                            
                          ),
                        ),

                    ),//Listtile

                      Container(
                        height: 70,
                            decoration: BoxDecoration(
                    border: Border(
                    
                      bottom: BorderSide(
                        color: Colors.grey[350],
                        width: 1.0,
                        
                      ),
                    ),
                  ),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: ListTile(
                            leading: Icon(Icons.supervised_user_circle,size: 30.0,),
                            title: Text("@Chhayhok",style: TextStyle(fontSize: 15.0),),
                            subtitle: Text("Username",style: TextStyle(fontSize: 13.0),),
                            
                          ),
                        ),
                    ),//Listtile

                  ],
                ),


              ),
            ),
          )

        ],
      ),
    );
  }
}
