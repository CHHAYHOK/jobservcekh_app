
import 'package:flutter/material.dart';

loading(BuildContext context){
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext ctx){
        return Dialog(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0)
          ),
          child: Container(
            height: 50.0,
            width: 50.0,
            padding: EdgeInsets.all(8.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: Center(child: CircularProgressIndicator()),
          ),
        );
      }
  );
}