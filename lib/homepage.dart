import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import './EditColor.dart';
import 'package:flutter_swiper/flutter_swiper.dart';


class homepage extends StatefulWidget {

  @override
  _homepageState createState() => _homepageState();
}
  

class _homepageState extends State<homepage> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  firebaseMessageConfig(){
 
    _firebaseMessaging.configure(
        onLaunch: (Map<String,dynamic> msg){
          print("OnLaunch $msg");
        },
        onResume: (Map<String,dynamic> msg){
          print("OnLaunch $msg");
        },
        onMessage: (Map<String,dynamic> msg){
          print("OnLaunch $msg");
        }

    );
    _firebaseMessaging.subscribeToTopic("Everyone");
    print("Firbase${_firebaseMessaging.getToken()}");
  }
 @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseMessageConfig();
  }

  @override
  Widget build(BuildContext context) {  
    // TODO: implement build
    return new Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: new AppBar(
        title: Center(child: new Text('Job Service')),
        backgroundColor:EditColor,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: Container(
              child: IconButton(
                  icon: Icon(
                    Icons.notifications,
                    size: 25,
                  ),
                  onPressed: () {}),
            ),
          )
        ],
      ),

      drawer: Drawer(
        child: Container(
          color: EditColor,
          child: new ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            children: <Widget>[
                              new Container(
                                height: 70.0,
                                width: 70.0,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: new ExactAssetImage("images/lii.jpg"),
                                    fit: BoxFit.cover,
                                  ),
                                  shape: BoxShape.circle,
                                ),
                              ),
                            ],
                          ), //Container
                        ), //padding
                        Row(
                          children: <Widget>[
                            new Text("Chhayhok",style:
                            TextStyle(fontSize: 17,color: Colors.white),),
                          ],
                        )
                      ],
                    ), //Column

                  ),

                ),
              ),

              new Divider(
                color: Colors.white,
              ),
              InkWell(
                onTap: (){
                  Navigator.of(context).pushNamed("/Profile");
                },
                child: ListTile(
                    leading: new Icon(
                      Icons.person,
                      color: Colors.white,
                    ),
                    title: new Text(
                      'Profile',
                      style: TextStyle(fontSize: 16.0, color: Colors.white),
                    ), //Text
                  ),
              ), //ListTile
              new Divider(
                color: Colors.white,
              ),
              ListTile(
                leading: new Icon(
                  Icons.phone,
                  color: Colors.white,
                ),
                title: new Text(
                  'Contact',
                  style: TextStyle(fontSize: 16.0, color: Colors.white),
                ), //Text
              ), //ListTile
              new Divider(
                color: Colors.white,
              ),
              ListTile(
                leading: new Icon(
                  Icons.turned_in,
                  color: Colors.white,
                ),
                title: new Text(
                  'Save',
                  style: TextStyle(fontSize: 16.0, color: Colors.white),
                ), //Text
              ), //ListTile
              new Divider(
                color: Colors.white,
              ),
              ListTile(
                leading: new Icon(
                  Icons.info,
                  color: Colors.white,
                ),
                title: new Text(
                  'About App',
                  style: TextStyle(fontSize: 16.0, color: Colors.white),
                ), //Text
              ), //ListTile
              new Divider(
                color: Colors.white,
              ),
              InkWell(
                onTap: (){
                  Navigator.of(context).pushNamed("/settings");                },
                   child: ListTile(
                  leading: new Icon(
                    Icons.settings,
                    color: Colors.white,
                  ),
                  title: new Text(
                    'Setting',
                    style: TextStyle(fontSize: 16.0, color: Colors.white),
                  ), //Text
                ),
              ), //ListTile
              new Divider(
                color: Colors.white,
              ),
            ],
          ),
        ),
      ),


      body: ListView(children: <Widget>[
    Container(
      height: 150,
      margin: EdgeInsets.symmetric(vertical: 10),
      child: new Swiper(
        autoplay: true,
      itemBuilder: (BuildContext context, int index) {

        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            image: DecorationImage(image:AssetImage("images/job.jpeg"),fit: BoxFit.cover )
          ),
          );
          },
  itemCount: 10,
  viewportFraction: 0.8,
  scale: 0.9,
),
    )
        // new SizedBox(
        //     height: 250.0,
        //     child: Container(
        //       child: Padding(
        //         padding: const EdgeInsets.all(8.0),
        //         child: ClipRRect(
        //           borderRadius: BorderRadius.circular(15),
        //         child: new Carousel(
        //             images: [
        //               new ExactAssetImage("images/job.jpeg"),
        //               new ExactAssetImage("images/job4.jpg"),
        //               new ExactAssetImage("images/job5.jpg"),
        //               new ExactAssetImage("images/job1.jpg"),
        //               new ExactAssetImage("images/job3.jpg"),
        //               new ExactAssetImage("images/job2.jpg"),
        //             ],
        //             dotBgColor: Colors.purple.withOpacity(0.0),
        //             dotSpacing: 15.0,dotColor:Colors.amber,
        //             //dotSize: 0.0,
        //           ),
        //         ) 
        //       ),
        //     )
        //     )
            ,
        new Container(
          height: 1.0,
          color: Colors.white,
        ),
        Container(
          child: GridView.count(
            crossAxisCount: 3,
            shrinkWrap: true,
            controller: ScrollController(keepScrollOffset: false),
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed("/listjob");
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border(),
                    
                  ),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                          alignment: Alignment.center,
                          child: Image.asset(
                            "images/joblist.png",
                            width: 55.0,
                            color: EditColor
                          )),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: new Text(
                          "JOB LIST",
                          style: TextStyle(
                            fontSize: 14.0,
                            color: EditColor,
                            fontFamily:'Pacifico',
                          
                          ),
                        ),
                      ),
                    ],
                  )),
                ), //Container
              ), //InkWell

              InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed("/jobcategory");
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border(
                        left: BorderSide(
                      color:EditColor,
                      width: 1.0,
                    )),
                  ),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                          alignment: Alignment.center,
                          child: Image.asset(
                            "images/category.png",
                            width: 55.0,
                            color:EditColor,
                          )),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: new Text(
                          "JOB CATEGORY",
                          style: TextStyle(
                            fontSize: 14.0,
                            color: EditColor,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                    ],
                  )),
                ),
              ), //Container
              InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed("/companylist");
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        color: EditColor,
                        width: 1.0,
                      ),
                    ),
                  ),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                          alignment: Alignment.center,
                          child: Image.asset(
                            "images/company.gif",
                            width: 55.0,
                            color: EditColor,
                          )),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: new Text(
                          "COMPANY LIST",
                          style: TextStyle(
                            fontSize: 14.0,
                            color: EditColor,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                    ],
                  )),
                ),
              ), //Container

              InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed("/joblocation");
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                          //                    <--- top side
                          color: EditColor,
                          width: 1.0,
                        ),
                        top: BorderSide(
                          color: EditColor,
                          width: 1.0,
                        )),
                  ),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                          alignment: Alignment.center,
                          child: Image.asset(
                            "images/location.png",
                            width: 55.0,
                            color: EditColor,
                          )),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: new Text(
                          "JOB LOCATION",
                          style: TextStyle(
                              fontSize: 14.0,
                              color: EditColor,
                              fontStyle: FontStyle.normal),
                        ),
                      ),
                    ],
                  )),
                ),
              ), //Container

              InkWell(
                onTap: (){
                  Navigator.of(context).pushNamed("/searchjob");
                },
                 child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        //<--- top side
                        color: EditColor,
                        width: 1.0,
                      ),
                      top: BorderSide(
                        color: EditColor,
                        width: 1.0,
                      ),
                      left: BorderSide(
                        color: EditColor,
                        width: 1.0,
                      ),
                    ),
                  ),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                          alignment: Alignment.center,
                          child: Image.asset(
                            "images/find.jpg",
                            width: 55.0,
                            color: EditColor,
                          )),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: new Text(
                          "SEARCH ",
                          style: TextStyle(
                            fontSize: 14.0,
                            color: EditColor,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                    ],
                  )),
                ),
              ), //Container
              InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed("/loginjsk");
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        //                    <--- top side
                        color: EditColor,
                        width: 1.0,
                      ),
                      top: BorderSide(
                        color: EditColor,
                        width: 1.0,
                      ),
                      left: BorderSide(
                        color: EditColor,
                        width: 1.0,
                      ),
                    ),
                  ),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                          alignment: Alignment.center,
                          child: Image.asset(
                            "images/jobsteker.png",
                            width: 55.0,
                            color: EditColor,
                          )),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: new Text(
                          "JOB SEEKER",
                          style: TextStyle(
                            fontSize: 14.0,
                            color: EditColor,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                    ],
                  )),
                ), //Container
              ), //InkWell
            ], 
          ), //Gridcount
        ),
       
          // new Container(
          //   height: 130,
          //   color: Colors.indigo,
          //     child: Image.asset("images/angko.png",fit:BoxFit.fill),          
          // ),
      ]), //ListView
    );
  }
}
