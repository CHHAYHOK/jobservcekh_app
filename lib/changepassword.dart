import 'package:findjob/EditColor.dart';
import 'package:flutter/material.dart';

class Changepass extends StatefulWidget{
  State<StatefulWidget> createState()=> new _ChangepassPageState();
}
class _ChangepassPageState extends State <Changepass>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(

      appBar: AppBar(title: Center(child: Text("Changes Password")),backgroundColor:EditColor,),

      body: ListView(
        children: <Widget>[
             new Container(
               margin: EdgeInsets.only(top: 40),
               child: Form(
                 child: Column(
                   children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: new TextField(
                         obscureText: true,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(15),
                           border: OutlineInputBorder(),
                          hintText: "Current"
                        ),
                         style: TextStyle(fontSize: 18),
                      ),
                      
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: new TextField(
                         obscureText: true,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(15),
                           border: OutlineInputBorder(),
                          hintText: "New"
                        ),
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: new TextField(
                         obscureText: true,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(15),
                           border: OutlineInputBorder(),
                          hintText: "Retype New "
                          
                        ),
                         style: TextStyle(fontSize: 18),
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.only(top: 23.0),
                        child: Container(
                          width: 390,
                          child: new RaisedButton(
                            color: Colors.amber,
                            padding: EdgeInsets.all(13.0),
                            child: new Text(
                              'Save Changes',
                              style: new TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.white,),
                            ),
                            onPressed: () {
                             
                              // Navigator.pop(context);
                            },
                          ),
                        ),
                      ), 
                        Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Container(
                          decoration: BoxDecoration(
                            border:Border.all(
                              width: 1,
                              color: Colors.grey,
                            )
                          ),
                          width: 390,
                          child: new RaisedButton(
                            padding: EdgeInsets.all(13.0),
                            child: new Text(
                              'Cancel',
                              style: new TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.black,),
                            ),
                            onPressed: () {
                             
                               Navigator.pop(context);
                            },
                          ),
                        ),
                      ), 
                   ],
                 ),
               ),
             )
        ],
      ),
    );
  }
}