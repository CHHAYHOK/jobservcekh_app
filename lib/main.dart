import 'package:findjob/detailjob.dart';
import 'package:findjob/searchjob.dart';
import 'package:findjob/settings.dart';
import 'package:findjob/splashscreen.dart';
import 'package:findjob/verifyCode.dart';
import 'package:flutter/material.dart';
import './homepage.dart';
import './splashscreen.dart';
import './registercom.dart';
import './loginjsk.dart';
import './registerjsk.dart';
import './jobsteeker.dart';
import './listjob.dart';
import './companylist.dart';
import './jobcategory.dart';
import './joblocation.dart';
import './profile.dart';
import './applycv.dart';
import './newaccapycv.dart';
import './loginfb.dart';
import './detailjob.dart';
import './profile.dart';
import './searchjob.dart';
import './forgetpass.dart';
import './newpassword.dart';
import './changepassword.dart';
import './loginepycom.dart';
void main(){
  
  runApp(new MaterialApp(

    debugShowCheckedModeBanner: false,
    home: new Splashscreen (),
    routes: <String, WidgetBuilder>{
      '/homepage': (BuildContext context) =>homepage(),
      //'/register':(BuildContext context)=>register() ,
      '/loginjsk':(BuildContext context)=>loginjsk(),
      '/registerjsk':(BuildContext context)=>registerjsk(),
      '/jobsteeker':(BuildContext context)=>jobsteeker(),
      '/listjob':(BuildContext context)=>listjob(),
      '/companylist':(BuildContext context)=>Comlist(),
      '/jobcategory':(BuildContext context)=>Jobcategory(),
      '/joblocation':(BuildContext context)=>Joblocation(),
      '/Profile':(BuildContext context)=>Profile(),
      // '/applycv':(BuildContext context)=>applycv(),
      //'/newaccapycv':(BuildContext context)=>newaccapycv(),
      '/loginfb':(BuildContext context)=>Loginfac(),
      '/settings':(BuildContext context)=>Settings(),
      '/searchjob':(BuildContext context)=>Searchjob(),
      '/forgetpass':(BuildContext context)=>Forgetpass(),
      '/changepassword':(BuildContext context)=>Changepass(),
   }//routes
  )
  );
}

