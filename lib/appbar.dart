import 'package:flutter/material.dart';
import 'EditColor.dart';

class Appbar extends StatefulWidget{
  State<StatefulWidget> createState() => _AppbarPageState();
}
class _AppbarPageState extends State<Appbar>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
     return Scaffold(
  appBar: AppBar(
        backgroundColor: EditColor,
        leading: InkWell(
          onTap: ()=>Navigator.pop(context),
          child: Row(
            children: <Widget>[
              Icon(Icons.arrow_back_ios),
              Text("Back")
            ],
          ),
        ),

       actions: <Widget>[   
          
            new IconButton(
                icon: Icon(Icons.search),
                iconSize: 30.0,
                onPressed: () {
                  
                }),
         
       ],

      ),
     );
 
  }
}

