import 'package:findjob/EditColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import './EditColor.dart';

class jobsteeker extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _jobsteekerPageState();
}

class _jobsteekerPageState extends State<jobsteeker> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
     // backgroundColor: Colors.blueGrey,
      appBar: AppBar(title: Center(child: Text("Job Seeker")),backgroundColor: EditColor,),
      body: ListView
        (children: <Widget>[
        
        InkWell(
          onTap: (){
            Navigator.of(context).pushNamed("/Profile");
          },
         
            child: Padding(
              padding: const EdgeInsets.all(3.0),
              child: Container(              
               decoration: BoxDecoration(
                 border: Border(                 
                   bottom: BorderSide(
                     color:Colors.grey[350],
                   width: 1.0,
                   ),
                   
                 ),
                 color: Colors.white,
               ),
                child: new ListTile(
                  leading: Container(
                
                      height: 40.0,
                      width: 40.0,
                      decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.red,
                          ),
                          color: Colors.red),
                      child: Icon(
                        Icons.account_circle,
                        size: 30.0,
                        color: Colors.white,
                      )
                  ), //Container

                  title: Text(
                    "Profile",
                    style: TextStyle(fontSize: 20.0, color: Colors.black87),
                  ),
                  trailing: new Icon(Icons.keyboard_arrow_right),
                ),
              ),
            ),
          ),
      
        InkWell(
          onTap: (){
            Navigator.of(context).pushNamed("/listjob");
          },
            child: Container(
                decoration: BoxDecoration(
                 border: Border(                 
                   bottom: BorderSide(
                     color:Colors.grey[350],
                   width: 1.0,
                   ),
                   
                 ),
                 color: Colors.white,
               ),
              child: Padding(
                padding: const EdgeInsets.all(3.0),
                child: new ListTile(
                  leading: Container(
                       height: 40.0,
                      width: 40.0,
                      decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.cyan,
                          ),
                          color: Colors.cyan),
                      child: Icon(
                        Icons.library_books,
                        size: 30.0,
                        color: Colors.white,
                      )), //Container

                  title: Text(
                    "Job List",
                    style: TextStyle(fontSize: 20.0, color: Colors.black87),
                  ),
                  trailing: new Icon(Icons.keyboard_arrow_right),
                ),
              ),
            ),
          
        ), //Card
         InkWell(
          onTap: (){
            Navigator.of(context).pushNamed("/");
          },
          
            child: Padding(
              padding: const EdgeInsets.all(3.0),
              child: Container(
                  decoration: BoxDecoration(
                 border: Border(                 
                   bottom: BorderSide(
                     color:Colors.grey[350],
                   width: 1.0,
                   ),
                   
                 ),
                 color: Colors.white,
               ),
                child: new ListTile(
                  leading: Container(
                       height: 40.0,
                      width: 40.0,
                      decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.amberAccent,
                          ),
                          color: Colors.amberAccent),
                      child: Icon(
                        Icons.notifications_active,
                        size: 30.0,
                        color: Colors.white,
                      )), //Container
                  title: Text(
                    "Notification",
                    style: TextStyle(fontSize: 20.0, color: Colors.black87),
                  ),
                  trailing: new Icon(Icons.keyboard_arrow_right),
                ),
              ),
            ),
          ),
       

        InkWell(
          onTap:(){
            Navigator.of(context).pushNamed("/applycv");
          },
         
            child: Padding(
              padding: const EdgeInsets.all(3.0),
              child: Container(
                  decoration: BoxDecoration(
                 border: Border(                 
                   bottom: BorderSide(
                     color:Colors.grey[350],
                   width: 1.0,
                   ),
                   
                 ),
                 color: Colors.white,
               ),
                child: new ListTile(
                  leading: Container(
                      height: 40.0,
                      width: 40.0,
                      decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.green,
                          ),
                          color: Colors.green),
                      child: Icon(
                        Icons.exit_to_app,
                        size: 30.0,
                        color: Colors.white,
                      )), //Container

                  title: Text(
                    "Apply CV",
                    style: TextStyle(fontSize: 20.0, color: Colors.black87),
                  ),
                  trailing: new Icon(Icons.keyboard_arrow_right),
                ),
              ),
            ),
          ),
       
           Padding(
            padding: const EdgeInsets.all(3.0),
            child: Container(
                decoration: BoxDecoration(
                 border: Border(                 
                   bottom: BorderSide(
                     color:Colors.grey[350],
                   width: 1.0,
                   ),
                   
                 ),
                 color: Colors.white,
               ),
              child: new ListTile(
                leading: Container(
                     height: 40.0,
                      width: 40.0,
                      decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.orange
                          ),
                          color: Colors.orange),
                      child: Icon(
                        Icons.search,
                        size: 30.0,
                        color: Colors.white,
                    )), //Container

                title: Text(
                  "Search",
                  style: TextStyle(fontSize: 20.0, color: Colors.black87),
                ),
                trailing: new Icon(Icons.keyboard_arrow_right),
              ),
            ),
          ),
       
       
        Padding(
            padding: const EdgeInsets.all(3.0),
            child: Container(
                decoration: BoxDecoration(
                 border: Border(                 
                   bottom: BorderSide(
                     color:Colors.grey[350],
                   width: 1.0,
                   ),
                   
                 ),
                 color: Colors.white,
               ),
              child: new ListTile(
                leading: Container(
                     height: 40.0,
                      width: 40.0,
                      decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.teal,
                          ),
                          color: Colors.teal),
                      child: Icon(
                        Icons.payment,
                        size: 30.0,
                        color: Colors.white,
                    )), //Container

                title: Text(
                  "CV List",
                  style: TextStyle(fontSize: 20.0, color: Colors.black87),
                ),
                trailing: new Icon(Icons.keyboard_arrow_right),
              ),
            ),
          ),
        
      
      InkWell(
          onTap: (){
            Navigator.of(context).pushNamed("/settings");
          },
         
          child: Padding(
            padding: const EdgeInsets.all(3.0),
            child: Container(
                decoration: BoxDecoration(
                 border: Border(                 
                   bottom: BorderSide(
                     color:Colors.grey[350],
                   width: 1.0,
                   ),
                   
                 ),
                 color: Colors.white,
               ),
              child: new ListTile(
                leading: Container(
                     height: 40.0,
                      width: 40.0,
                      decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.lightBlue,
                          ),
                          color: Colors.lightBlue),
                      child: Icon(
                        Icons.settings,
                        size: 30.0,
                        color: Colors.white,
                    )), //Container

                title: Text(
                  "Setting",
                  style: TextStyle(fontSize: 20.0, color: Colors.black87),
                ),
                trailing: new Icon(Icons.keyboard_arrow_right),
              ),
            ),
          ),
      ),
    ]), //ListView
    );
  }
}
