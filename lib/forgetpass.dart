import 'package:findjob/newpassword.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart';
import './laoding.dart';
import './verifyCode.dart';
import './EditColor.dart';
class Forgetpass extends StatefulWidget{

  State<StatefulWidget> createState()=> new _ForgetpassState();
}

class _ForgetpassState extends State<Forgetpass>{
   TextEditingController phone = new TextEditingController();
   
   @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
       appBar: new AppBar(
        title: new Text('Forget Password'),
        centerTitle: true,
        backgroundColor: EditColor,
      ),
      body: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Card(
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top:50.0),
                child: new Container(
                
                height: 150,
                  //color: Colors.redAccent,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.asset("images/sms.png",color: EditColor,),
                  ),
          ),
              ),
                 Padding(
                padding: const EdgeInsets.only(top:10.0,),
                child: new Container(
                height: 70,
                  //color: Colors.redAccent,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Get a verification code by text messages",style: TextStyle(fontSize: 20),)
                  ),
          ),
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: new Form(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 12.0),
                        child: new TextField(
                          autofocus: true,
                          decoration: new InputDecoration(
                             contentPadding: EdgeInsets.all(8),
                              border: OutlineInputBorder(),
                              labelText: 'Enter Phone Number',
                              prefixIcon: Icon(Icons.phone_iphone,)
                              
                              ),
                          style: TextStyle(fontSize: 17.0, color: Colors.black),
                          controller: phone,
                          keyboardType: TextInputType.phone,
                        ),
                      ), //TextFormField
                       Padding(
                        padding: const EdgeInsets.only(top: 23.0),
                        child: new RaisedButton(
                          color: Colors.amber,
                          padding: EdgeInsets.all(13.0),
                          child: new Text(
                            'Send Text Messages',
                            style: new TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,),
                          ),
                          onPressed: () {
                            sendPhoneCode();
                            // Navigator.pop(context);
                          },
                        ),
                      ), //Padding

                    ], //Widget
                  ), //Column
                ), //Form
              ),
            ],
          ),
        ),
      ),
    ); //Scaffold
  }
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String verificationId;
  sendPhoneCode(){
    loading(context);
    final PhoneVerificationCompleted verificationCompleted = (AuthCredential credentail) {
      Navigator.pop(context);
      Navigator.of(context).push(
          MaterialPageRoute(
              builder: (BuildContext ctx)=>Newpass(),
          )
      );
      print('Inside _sendCodeToPhoneNumber: signInWithPhoneNumber auto succeeded: $credentail');
    };

    final PhoneVerificationFailed verificationFailed = (AuthException authException) {
      Navigator.pop(context);
      //Toast.show("The number format you enter is not correct.", context,duration: 3);
    };

    final PhoneCodeSent codeSent = (String verificationId, [int forceResendingToken]) async {
      this.verificationId = verificationId;
      Navigator.pop(context);
      Navigator.of(context).push(
          MaterialPageRoute(
              builder: (BuildContext ctx)=> VerifyCord(verificationId: this.verificationId,phone: this.phone.text,)
          )
      );
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout = (String verificationId) {
      this.verificationId = verificationId;
      print("time out");
    };
    _auth.verifyPhoneNumber(
        codeSent: codeSent,
        phoneNumber: this.phone.text,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        timeout: Duration(seconds: 60)
    );
  }
}